### SQL Basics 

This is a repo to introduce SQL and DBs!


Objectives:

- Intro to sql and db
- Install mysql and workbench
- Define a table and columns


## Introduction to SQL and DBs

What is a database?
- a file with loads of information
- Excel with contacts
- recipes in a book?

These can all be considered "databases" but really they are not very organised or easy to use, they have limitations. In tech these would be what people are refering to as DBs.

Usually in tech a DB is either:
- SQL or
- NoSQL (not only SQL)

**SQL** Structure Query Language 

## How is it structured:

- It has structured tables with connections between tables, imagine excel sheets connected to one another.

- This makes it easy to query and get out the information that we want.

## Common terminology:

- DB: Database
- SQL: Structure query langauge- how a DB might be organised 
- DBMS: Database Management System

Inside a SQL DB you will have

**Columns**
**Rows**
**Tables**

<u>Books</u>

| title                  | author_id | date | price | genre     |
|------------------------|-----------|------|-------|-----------|
| Harry Potter           | 1         | 2001 | 12    | Fiction   |
| Harry Potter 2         | 1         | 2003 | 12    | Fiction   |
| Good Vibes - Good Life | 2         | 2020 | 20    | Self Help |


<u>Authors</u>

| First_name | Last_name | email              |   |   |
|------------|-----------|--------------------|---|---|
| J. K.      | Rowling   | jk@gmail.com       |   |   |
| Harry      | Bookster  | hb@bookster.com    |   |   |
| Vex        | King      | king@kingbooks.com |   |   |


<u>Genre</u>

| type      | description                                 | other_info |   |
|-----------|---------------------------------------------|------------|---|
| fiction   | these are fictional noval                   | none       |   |
| Biography | some what true accounts of people's lives   | none       |   |
| Selfhelp  | Meant to help you think and better yourself | none       |   |


We can see how the above connect because we are machines that see context in everything.
But for a machine that is silicone based we need to use **Primary Keys** and **Foreign Keys**

**Primary Keys**
- Every single table has to have a column that cannot be empty, that is the primary key.
- Primary keys are unique and cannot be re-used (at least in good sql dbs)

**Foreign Keys**
- Foreign keys are a reference a primary key, they are essentially the primary key in a secondary table.

## Lets see the above example with primary keys:

Let's see the above example with primary keys:

| book_id  | title                  | author_id | date | price | genre     |
|----------|------------------------|-----------|------|-------|-----------|
| 1        | Harry Potter           | 1         | 2001 | 12    | fiction   |
| 2        | Harry Potter 2         | 1         | 2003 | 12    | fiction   |
| 3        | Good Vibes - Good life | 3         | 2020 | 20    | Self Help |


Authors

| author_id | First_name | Last_name | email              |   |   |
|-----------|------------|-----------|--------------------|---|---|
| 1         | J. K.      | Rowling   | jk@gmail.com       |   |   |
| 2         | Harry      | Bookster  | hb@bookster.com    |   |   |
| 3         | Vex        | King      | king@kingbooks.com |   |   |


#### The keys create the Relationships:
- one to one 1:1
- one to many 1:N
- many to many N:N

You can then make entity diagrams:
- I need a DB to store "CD's". I want the title, price and author.
- I also want more information on the author- email, phone number, first and last name.
- I want to be able to sell the CDs- keeping record of how many CDs we sold.

<u>CD TABLE:</u>

CD_id (PK)
Title
Price
Author_id (FK)

--> N:1

<u>AUTHOR TABLE:</u>

author_id (FK)
email
phone
first Name
last Name


<u>USERS:</u>
user_id
email
login
f_name
l_name

<u>SALES:</u> N:N Table
sale_id (pk)
cd_id (fk)
user_id (fk)



### SQL Data Types:

There are different types-
- CharIndex
- Charmax
- Decimals
- Integers

We'll look at the difference!

exercise: design a DB and define what is the best data type for each column. 

### MYSql and Maria DB
Once you have installed, enabled and started mysql or mariadb

```bash
mysql -u root -p -h localhost

```


### DDL Data Definition Language

creating tables and columns

```sql
-- Create a my_db
CREATE DATABASE my_db;

-- Create a table
CREATE TABLE film_table (
film_name VARCHAR(60);
date_of_release DATE;
description VARCHAR(320)
);

```

In SQL we need to define the data types for columns and the following exit:
- VARCHAR(n) - adaptable to different character lengths 
- VARCHAR(max) - same as above with maximum length
- CHARACTER OR CHAR - always takes the exact size
- Int- integers- whole numbers
- DATE, TIME, DATETIME
- DECIMAL or NUMERIC - defines the number of digits to the right of the decimal
- BINARY - used to story binary info such as pictures or music
- FLOAT - for very large numbers
- BIT - similar to binary (0, 1, NULL)



### Altering Tables
```SQL
-- add nationality to director table 

-- add directorID as a foreign key in film_table

-- alter film table so that title cannot be null.

-- alter directors table to add a description to each director

-- alter table to remove description to each director.

-- what is a dependant destroy constraint and how is it useful?




```


### Data Manipulation Language 

inserting, deleting and altering rows

- INSERT INTO <table> (directorID, first_name, last_name) VALUES (10, 'Nicole', 'Ghessen')
- DELETE FROM <table> WHERE COLUMN = <ID>
- UPDATE FROM <table> WHERE COLUMN = <ID>




### Data Query Language 
this one is to retrieve data 

```SQL

-- normal SELECT clause and FROM clause
SELECT <column> FROM <table>
-- normal select clause and from clause + where clause
SELECT <column> FROM <table> WHERE id =id

-- operators
SELECT <column> FROM <table> WHERE price = x
SELECT <column> FROM <table> WHERE price != x
SELECT <column> FROM <table> WHERE price < x
SELECT <column> FROM <table> WHERE price > x

-- logical and /or in select
SELECT <column> FROM <table> WHERE price = x AND category ID =1 -- both sides need to be true to get said value

SELECT <column> FROM <table> WHERE price = x OR categoryid =1 -- only one side needs to be true to get value.

-- matcher and wild cards % [^charlist] [charlist]


-- % is thr wild card for any number of characters and  any character.
SELECT <column> FROM <table> WHERE name LIKE 'J%'
SELECT <column> FROM <table> WHERE name LIKE '%ss'


-- is the substitute for one single character
SELECT <column> FROM <table> WHERE name LIKE '____'

-- [charlist] % match

-- [^charlist] % don't match


-- WHERE Between
SELECT * FROM [Products] WHERE PRICE BETWEEN 10 AND 22

-- in ('lists', 'items')
SELECT * FROM [customers] WHERE country IN ('Germany', 'Mexico')

-- Alias- for columns and agregate data
SELECT CustomerID AS ID, CustomerName AS 'Company Name', ContactName AS 'Humanoid' FROM [customers] WHERE country IN ('Germany', 'Mexico')

-- can also alias the creation of new data
-- figuring out how much to pay for 30 new of each product in categoryid 1
SELECT *, price * 30 AS '£ to Order' FROM [Products] WHERE CategoryID = 1 

-- order the results in descending order:
SELECT *, price * 30 AS '£ to Order' FROM [Products] WHERE CategoryID = 1 ORDER BY SupplierID DESC;

-- selecting particular data based on price
SELECT MAX(price)as 'maximum price', MIN(price)as 'minimum price', AVG(price) as 'average price', COUNT(price) FROM [Products]


-- Can be segregated by GROUP BY STATEMENT 
SELECT SupplierID, MAX(price)as 'maximum price', MIN(price)as 'minimum price', AVG(price) as 'average price', COUNT(price) FROM [Products]GROUP by SupplierID

-- Can't use aggregate functions and 'WHERE' together it will break have to use HAVING:if using HAVING then use GROUP BY after
SELECT SupplierID, AVG(price), COUNT(price) FROM [Products] GROUP BY SupplierID HAVING Price >10

```

- Play around with SQL querying on this site: https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_all

## Sometimes you want to join data: Join quieries allow you to get data from several tables.
- start with selecting the first table 
- join the second, specify where the data meets

```SQL
-- Inner Joints
SELECT ProductID, ProductName, Products.CategoryID, CategoryName FROM [Products]
JOIN Categories ON Products.CategoryID = Categories.CategoryID

--Inner joint syntax
SELECT <columns> FROM <table1>
JOIN <table2> 
ON <table1>.someId = <table2>.someID


## Exercise 
-- I want to see all my orders with my customer information. Please get that data with Contact name and city
SELECT ContactName, City FROM [ORDERS]
JOIN Customers
ON Orders.CustomerID = Customers.CustomerID

--This is GREAT you are my AL SQL guru's! I know need more information. I'm actually. trying to figure out where to go for a sales meeting. Can you get me all of the above information but only from customers in Madrid?
SELECT ContactName, City FROM [ORDERS]
JOIN Customers
ON Orders.CustomerID = Customers.CustomerID
WHERE City = 'Madrid'


-- Third part of exercise: I need to know which city is doing more orders. Get all the order and the client information and aggregate by city

SELECT COUNT(*) as 'number or orders', CITY FROM [Orders]
Join Customers
ON Customers.Customerid = Orders.CustomerID 
GROUP BY CITY
ORDER BY COUNT(*) DESC
```




























Syntax:

```SQL

SELECT <columns> FROM <TABLE>;


# example to get all customer data

SELECT * FROM Customers;

# if i want from cutomer, the customername, address and city

SELECT CustomerName, Address, City FROM [Customers]


# to filter a specific item in the table you:

SELECT * FROM [Orders] WHERE  OrderID=10249
# this would then pull up all the information about the OrderID:10249


# To get the number of customers in the USA from our example database 
SELECT COUNT (*) FROM (Customers) WHERE Country = "USA";

